# ContextualCode\GroupSearchBundle

This bundle contains a base implementation of user search in multiple groups.

## Prerequisite

Be aware that this package overrides default eZ Platform Solr services to support highlights.
If you have these classes override in your project, make sure you have this 

    ```yaml
    ezpublish.search.solr.gateway.native.class: ContextualCode\GroupSearchBundle\eZ\Solr\Gateway\Native
    ezpublish.search.solr.result_extractor.native.class: ContextualCode\GroupSearchBundle\eZ\Solr\NativeResultExtractor
    ```

## Installation

1. Include this bundle in your composer.json:

    ```yml
    {
        "require": {
            "contextualcode/ezplatform-group-search-bundle": "dev-master"
        },
        "repositories": [
            {
                "type": "vcs",
                "url":  "git@gitlab.com:contextualcode/ezplatform-group-search-bundle.git"
            }
        ]
    }
    ```

2. Run `composer update contextualcode/ezplatform-group-search-bundle`

3. Enable this bundle in `app/AppKernel.php`:

    ```php
    
    public function registerBundles()
    {
        $bundles = [
            ...,
            new ContextualCode\GroupSearchBundle\ContextualCodeGroupSearchBundle(),
        ];
    }
    ```

4. Make sure to add these lines to `app/config/routing.yml`:
  
    ```yaml
   group_search_routes:
       resource: "@ContextualCodeGroupSearchBundle/Resources/config/routing.yml"
    ``` 

5. (optional) To use JS part from the package, register entrypoint in `webpack.config.js`:
    
    ```javascript
    Encore.addEntry('group_search', './vendor/contextualcode/ezplatform-group-search-bundle/src/ContextualCode/GroupSearchBundle/Resources/public/standard/group-search.js');
    ``` 

    Then Run encore:

    ```bash
    yarn encore [dev|prod]
    ```
   
   And include in template:
   
    ```twig
    {{ encore_entry_script_tags('group_search') }}
    ```

6. (optional) Define `ezsettings.default.contextualcode.group_search.groups_definition` parameter.
    Default configuration:

    ```yaml
    ezsettings.default.contextualcode.group_search.groups_definition:
        general:
            title: Search Results
            no_results_message: '<li>No related pages found for {term}</li>'
            exclude_content_types: ['image', 'user', 'user_group']
            max_count: 25
            parent_location_path: /1/2/
        images:
            title: Images
            no_results_message: '<li>No Images found for {term}</li>'
            include_content_types: ['image']
            max_count: 10
            parent_location_path: /1/43/
    ```

7. In template, render search input block. For example:

    ```twig
    <button onclick="document.getElementById('input_block').style.display = 'block'">Show</button>
    <div id="input_block" style="display: none">
        <button onclick="document.getElementById('input_block').style.display = 'none'">Hide</button>
        {{ render(controller('ContextualCodeGroupSearchBundle:GroupSearch:inputBlock')) }}
    </div>
    ```

8. Done!
