<?php

namespace ContextualCode\GroupSearchBundle\Controller;

use ContextualCode\GroupSearchBundle\Service\GroupSearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GroupSearchController extends AbstractController
{
    /**
     * @return Response
     */
    public function inputBlockAction(): Response
    {
        $groups = [];
        foreach ($this->get('contextualcode.group_search.service')->getGroupsConfig() as $id => $group) {
            $groups[] = [
                'identifier' => $id,
                'no_results_message' => $group['no_results_message'] ?? null,
                'title' => $group['title'],
            ];
        }

        return $this->render('@ezdesign/group_search_block.html.twig', ['groups' => $groups]);
    }

    /**
     * @Route("/group_search", name="group_search", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function searchAction(Request $request): JsonResponse
    {
        $term = trim($request->query->get('term', ''));

        $groupSearch = $this->get('contextualcode.group_search.service');

        if ($request->query->get('includePatternReply', false)) {
            $patternResults = $groupSearch->findReplyBySearchPattern($term);
            if ($patternResults) {
                return new JsonResponse([
                    'patternResults' => $patternResults,
                    'groupedResults' => null,
                ]);
            }
        }

        $groupedResults = [];
        foreach (array_keys($groupSearch->getGroupsConfig()) as $identifier) {
            $groupedResults[] = [
                'identifier' => $identifier,
                'items' => $groupSearch->findGroupItems($identifier, $term),
            ];
        }

        return new JsonResponse(['groupedResults' => $groupedResults, 'patternResults' => null]);
    }

    public static function getSubscribedServices(): array
    {
        return [
            'contextualcode.group_search.service' => GroupSearchService::class,
        ];
    }
}
